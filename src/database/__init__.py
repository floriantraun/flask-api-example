from flask_sqlalchemy import SQLAlchemy

# Stick to conventions when moving databases.
from sqlalchemy import MetaData

naming_convention = {
	"ix": 'IX_%(column_0_label)s',  # Index
	"uq": "UQ_%(table_name)s_%(column_0_name)s",  # Unique Constraint
	"ck": "CK_%(table_name)s_%(column_0_names)s",  # Check
	"fk": "FK_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",  # Foreign Key
	"pk": "PK_%(table_name)s"  # Primary Key
}

metadata = MetaData(naming_convention=naming_convention)
db = SQLAlchemy(metadata=metadata)
