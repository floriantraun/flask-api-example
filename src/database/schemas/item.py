from database.models.item import ItemModel
from . import ma, dump_only_preset


class ItemSchema(ma.ModelSchema):
	class Meta:
		model = ItemModel

		load_only = ("store",)  # Never dump.
		dump_only = dump_only_preset  # Never require on input data.
		include_fk = True  # Include foreign keys.
