from marshmallow import pre_load

from database.models.user import UserModel
from . import ma, dump_only_preset


class UserSchema(ma.ModelSchema):
	class Meta:
		model = UserModel

		load_only = ("password", "confirmation_code")  # Never dump.
		dump_only = dump_only_preset + (
			"activated", "confirmation_code", "profile_image")  # Never require on input data.

	@pre_load
	def serialize_pre_load(self, data, **kwargs):
		data["username"] = data["username"].lower().strip()
		return data
