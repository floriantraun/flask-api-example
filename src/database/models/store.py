"""
Not essential for the core of the app (users, tokens, ...) to work. Feel free to modify & delete. Don't forget to also
remove the resource and schema.
"""

from database import db
from . import Model


class StoreModel(Model):
	__tablename__ = "stores"

	name = db.Column(db.String(80), nullable=False)

	items = db.relationship("ItemModel", lazy="dynamic")

	@classmethod
	def find_by_name(cls, name: str) -> "StoreModel":
		"""
		Looks up a store by his name.
		:param name: store name
		:return: StoreModel object
		"""
		return cls.query.filter_by(name=name).first()
