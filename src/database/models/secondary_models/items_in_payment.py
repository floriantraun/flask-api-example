from database import db
from .. import Model


class ItemsInPayment(Model):
	__tablename__ = "items_in_payment"

	item_id = db.Column("item_id", db.Integer, db.ForeignKey("items.id"))
	order_id = db.Column("payment_id", db.Integer, db.ForeignKey("payments.id"))
	quantity = db.Column("quantity", db.Integer, nullable=False)
	price = db.Column("price", db.Float(precision=2), nullable=False)

	item = db.relationship("ItemModel")
	payment = db.relationship("PaymentModel", back_populates="items")
