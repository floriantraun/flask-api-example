import stripe

from database import db
from libs.config import getconf
from . import Model
from .secondary_models.items_in_payment import ItemsInPayment

CURRENCY = "eur"


class PaymentModel(Model):
	__tablename__ = "payments"

	status = db.Column(db.String(20), nullable=False)

	items = db.relationship(ItemsInPayment, back_populates="payment")

	@classmethod
	def find_by_status(cls, status: str) -> "PaymentModel":
		return cls.query.filter_by(status=status)

	def set_status(self, status: str) -> None:
		self.status = status
		self.save()

	def charge_with_stripe(self, token: str) -> stripe.Charge:
		stripe.api_key = getconf("stripe", "secret_key")

		return stripe.Charge.create(
			amount=self.amount(),
			currency=CURRENCY,
			description=self.description(),
			source=token
		)

	def amount(self) -> int:
		total = 0

		for item in self.items:
			total += item.quantity * item.price

		return int(total * 100)

	def description(self) -> str:
		"""
		Generating a simple string representing this payment.
		:return: str
		"""
		item_counts = [f"{i.quantity}x {i.item.name}" for i in self.items]
		return ", ".join(item_counts)
