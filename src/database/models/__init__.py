from datetime import datetime
from typing import List

from database import db


class Model(db.Model):
	__abstract__ = True

	id = db.Column(db.Integer, primary_key=True)
	created_at = db.Column(db.DateTime(timezone=True), default=datetime.utcnow)
	updated_at = db.Column(db.DateTime(timezone=True), default=datetime.utcnow, onupdate=datetime.utcnow)

	@classmethod
	def find_all(cls) -> List:
		"""
		Returns all rows.
		:return:
		"""
		return cls.query.all()

	@classmethod
	def find_by_id(cls, _id: int) -> db.Model:
		"""
		Looks up a row by its ID.
		:param _id: ID
		:return: Model object
		"""
		return cls.query.filter_by(id=_id).first()

	def save(self) -> None:
		"""
		Upserting rows.
		:return: None
		"""
		db.session.add(self)
		db.session.commit()

	def delete(self) -> None:
		"""
		Deleting rows.
		:return: None
		"""
		db.session.delete(self)
		db.session.commit()
