"""
Not essential for the core of the app (users, tokens, ...) to work. Feel free to modify & delete. Don't forget to also
remove the resource and schema.
"""

from database import db
from . import Model
from .store import StoreModel


class ItemModel(Model):
	__tablename__ = "items"

	name = db.Column(db.String(80), nullable=False)
	price = db.Column(db.Float(precision=2), nullable=False)

	store_id = db.Column(db.Integer, db.ForeignKey("stores.id"), nullable=False)
	store = db.relationship(StoreModel)

	@classmethod
	def find_by_name(cls, name: str) -> "ItemModel":
		"""
		Looks up an item by his name.
		:param name: item name
		:return: ItemModel object
		"""
		return cls.query.filter_by(name=name).first()

	@classmethod
	def find_all_by_store_id(cls, store_id: int) -> "ItemModel":
		"""
		Looks up items by its store_id.
		:param store_id: Store ID
		:return: ItemModel object
		"""
		return cls.query.filter_by(store_id=store_id).all()
