"""
Provides the config used by flask.Flask to configure the app.
"""

import os

from libs.config import getconf

SECRET_KEY = getconf("app", "app_secret")  # Flask
JWT_SECRET_KEY = getconf("app", "jwt_secret")  # Flask JWT
DEBUG = getconf("app", "debug")  # Flask
TESTING = getconf("app", "debug")  # Flask
ENV = getconf("app", "env")  # Flask
SQLALCHEMY_DATABASE_URI = getconf("db", "db_uri")  # SQLAlchemy
SQLALCHEMY_TRACK_MODIFICATIONS = False  # SQLAlchemy
BUNDLE_ERRORS = True  # reqparse
JWT_BLACKLIST_ENABLED = True  # JWT Extended
JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']  # JWT Extended,
PROPAGATE_EXCEPTIONS = True  # Let Flask see errors raised
UPLOADED_UPLOADS_DEST = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..", "static",
                                     "uploads")  # Flask-Uploads
