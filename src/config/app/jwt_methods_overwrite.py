from flask_jwt_extended import JWTManager

from database.models.revoked_tokens import RevokedTokensModel
from libs.lang import gettext


def jwt_methods_overwrite(jwt: JWTManager) -> None:
	"""
	Provides methods to overwrite the default JWT methods.
	:param jwt: flask_jwt_extended.JWTManager
	:return: None
	"""

	@jwt.expired_token_loader
	def expired_token_callback():
		"""
		Replaces the default action taken whenever a token expired.
		:return: Flask response
		"""
		return {
			       "message": gettext("_app", "TOKEN_EXPIRED_ERROR"),
			       "error": "TOKEN_EXPIRED"
		       }, 401

	@jwt.token_in_blacklist_loader
	def check_if_token_in_blacklist(decrypted_token: dict):
		"""
		Checks if a token is in blacklist (revoked). Will not work if JWT_BLACKLIST_ENABLED is set to False.
		:param decrypted_token:
		:return: boolean
		"""
		jti = RevokedTokensModel.find_by_jti(decrypted_token['jti'])
		return False if not jti else True

	# Customizing JWT response/error messages.
	@jwt.invalid_token_loader
	def invalid_token_callback(msg: str):
		"""
		Replaces the default action taken whenever a token is invalid.
		:param msg: Error description
		:return: Flask response
		"""
		return {
			       "message": gettext("_app", "TOKEN_INVALID_ERROR").format(msg),
			       "error": "TOKEN_INVALID"
		       }, 401

	@jwt.unauthorized_loader
	def unauthorized_token_callback(msg: str):
		"""
		Replaces the default action taken whenever a resource is accessed unauthorized.
		:param msg: Error description
		:return: Flask response
		"""
		return {
			       "message": gettext("_app", "UNAUTHORIZED_ERROR").format(msg),
			       "error": "UNAUTHORIZED"
		       }, 401

	@jwt.needs_fresh_token_loader
	def needs_fresh_token_callback():
		"""
		Replaces the default action taken whenever a resource needs a fresh token.
		:return: Flask response
		"""
		return {
			       "message": gettext("_app", "FRESH_TOKEN_REQUIRED_ERROR"),
			       "error": "FRESH_TOKEN_REQUIRED"
		       }, 401

	@jwt.revoked_token_loader
	def revoked_token_callback():
		"""
		Replaces the default action taken whenever a revoked token is being used.
		:return: Flask response
		"""
		return {
			       "message": gettext("_app", "TOKEN_REVOKED_ERROR"),
			       "error": "TOKEN_REVOKED"
		       }, 401
