from flask_restful import Api

from resources.item.item import Item
from resources.item.item_list import ItemList
from resources.payment.payment import Payment
from resources.store.store import Store
from resources.store.store_list import StoreList
from resources.user.image_upload import ImageUpload
from resources.user.token_refresh import TokenRefresh
from resources.user.user import User
from resources.user.user_auth import UserLogin
from resources.user.user_change_password import ChangePassword
from resources.user.user_confirm import UserConfirm
from resources.user.user_logout import UserLogout
from resources.user.user_oauth_github import GithubLogin, GithubAuthorize
from resources.user.user_register import UserRegister
from resources.user.user_set_password import SetPassword


def add_resources(api: Api) -> None:
	"""
	Adds the resources to the API.
	:param api: flask_restful.Api
	:return: None
	"""
	api.add_resource(Store, "/store/<string:name>")
	api.add_resource(StoreList, "/stores")

	api.add_resource(Item, "/item/<string:name>")
	api.add_resource(ItemList, "/items")

	api.add_resource(User, "/user/<int:user_id>")
	api.add_resource(SetPassword, "/user/set-password")
	api.add_resource(ChangePassword, "/user/change-password")
	api.add_resource(ImageUpload, "/user/upload-image")
	api.add_resource(UserLogin, "/user/auth")
	api.add_resource(GithubLogin, "/user/auth/github")
	api.add_resource(GithubAuthorize, "/user/auth/github/authorized", endpoint="github.authorized")
	api.add_resource(UserLogout, "/user/logout")
	api.add_resource(UserConfirm, "/user/confirm/<string:confirmation_code>")
	api.add_resource(UserRegister, "/user/register")
	api.add_resource(TokenRefresh, "/user/refresh")

	api.add_resource(Payment, "/payment")
