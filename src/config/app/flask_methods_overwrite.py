from flask import Flask
from marshmallow import ValidationError

from libs.lang import gettext


def flask_methods_overwrite(app: Flask) -> None:
	"""
	Provides methods to overwrite the default Flask methods such as error handling.
	:param app: flask.Flask
	:return: None
	"""

	@app.errorhandler(ValidationError)
	def handle_marshmallow_validation_error(err: ValidationError):
		"""
		Replaces the default action taken whenever a marshmallow ValidationError Exception is thrown.
		:return: Flask response
		"""
		return {
			       "message": gettext("_app", "MA_VALIDATION_ERROR"),
			       "error": "VALIDATION_ERROR",
			       "fields": err.messages
		       }, 400
