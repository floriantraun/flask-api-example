from flask import request, url_for
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource
from flask_uploads import UploadNotAllowed, IMAGES

from database.models.user import UserModel
from libs import upload_helper
from libs.lang import gettext
from libs.upload_helper import is_filename_safe
from . import file_schema


class ImageUpload(Resource):
	@classmethod
	@jwt_required
	def post(cls):
		"""
		Used to upload a file.
		Uses JWT to retrieve user information and then saves the image to the `static/uploads/users` folder.
		If there is a filename conflict, it appends a number at the end.
		:return:
		"""
		data = file_schema.load(request.files)  # {"file": FileStorage}
		extension = upload_helper.get_extension(data["file"])

		try:
			if is_filename_safe(data["file"], IMAGES):
				file_path = upload_helper.save_file(data["file"], folder="users")
				basename = upload_helper.get_basename(file_path)

				user = UserModel.find_by_id(get_jwt_identity())
				user.profile_image = file_path
			else:
				return {
					       "message": gettext("_app", "ILLEGAL_EXTENSION_ERROR").format(extension),
					       "error": "ILLEGAL_EXTENSION"}, 400

			try:
				user.save()
			except:
				return {
					       "message": gettext("_app", "UNDEFINED_ERROR"),
					       "error": "UNDEFINED_ERROR"}, 500

			return {
				       "message": gettext("user", "IMAGE_UPLOADED").format(basename),
				       "url": url_for("static", filename=f"uploads/{file_path}", _external=True)
			       }, 201
		except UploadNotAllowed:
			return {
				       "message": gettext("_app", "ILLEGAL_EXTENSION_ERROR").format(extension),
				       "error": "ILLEGAL_EXTENSION"
			       }, 400
