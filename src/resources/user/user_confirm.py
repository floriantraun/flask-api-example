from flask_restful import Resource

from database.models.user import UserModel
from libs.lang import gettext


class UserConfirm(Resource):
	@classmethod
	def get(cls, confirmation_code: str):
		user = UserModel.find_by_confirmation_code(confirmation_code)

		if not user:
			return {
				       "message": gettext("user", "CONFIRMATION_CODE_NOT_FOUND_ERROR"),
				       "error": "NOT_FOUND"
			       }, 404

		user.activated = True
		user.save()
		return {
			       "message": gettext("user", "USER_CONFIRMED")
		       }, 200
