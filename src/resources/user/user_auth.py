from flask import request
from flask_jwt_extended import create_access_token, create_refresh_token
from flask_restful import Resource
from werkzeug.security import check_password_hash

from database.models.user import UserModel
from libs.lang import gettext
from . import user_schema


class UserLogin(Resource):
	@classmethod
	def post(cls):
		"""
		Authenticates user and creates access and refresh tokens.
		:return:
		"""
		data = user_schema.load(request.get_json())
		user = UserModel.find_by_username(data.username)

		if user and user.password:
			if check_password_hash(pwhash=user.password, password=data.password):
				if not user.activated:
					return {
						"message": gettext("user", "USER_NOT_ACTIVATED_ERROR"),
						"error": "USER_NOT_ACTIVATED"
					}

				access_token = create_access_token(identity=user.id, fresh=True)
				refresh_token = create_refresh_token(identity=user.id)

				return {
					       "access_token": access_token,
					       "refresh_token": refresh_token
				       }, 200

		return {
			       "message": gettext("user", "INVALID_CREDENTIALS_ERROR"),
			       "error": "INVALID_CREDENTIALS"
		       }, 401
