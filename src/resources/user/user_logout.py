from flask_jwt_extended import jwt_required, get_raw_jwt
from flask_restful import Resource

from libs.lang import gettext
from . import revoked_token_schema


class UserLogout(Resource):
	@classmethod
	@jwt_required
	def post(cls):
		"""
		Logs a user out and revokes the tokens.
		:return:
		"""
		jti = get_raw_jwt()['jti']  # jti: JWD ID, unique identifier
		revoked_jti = revoked_token_schema.load({"jti": jti})

		try:
			revoked_jti.save()
		except:
			return {
				       "message": gettext("user", "LOGOUT_ERROR"),
				       "error": "UNDEFINED_ERROR"
			       }, 500

		return {
			       "message": gettext("user", "LOGOUT")
		       }, 200
