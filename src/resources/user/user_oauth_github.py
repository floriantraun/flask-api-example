from flask import g, request, url_for
from flask_jwt_extended import create_access_token, create_refresh_token
from flask_restful import Resource

from database.models.user import UserModel
from libs.lang import gettext
from libs.oauth import github


class GithubLogin(Resource):
	@classmethod
	def get(cls):
		return github.authorize(url_for("github.authorized", _external=True))


class GithubAuthorize(Resource):
	@classmethod
	def get(cls):
		response = github.authorized_response()

		if response is None or response.get("access_token") is None:
			return {
				"message": request.args["error_description"],
				"error": request.args["error"]
			}

		g.access_token = response["access_token"]

		github_user = github.get("user")

		if not github_user.data["email"]:
			return {
				"message": gettext("user", "EMAIL_INVALID_ERROR"),
				"error": "EMAIL_INVALID_ERROR"
			}

		user = UserModel.find_by_username(github_user.data["email"])

		if not user:
			user = UserModel(username=github_user.data["login"], password=None, activated=True)
			user.save()

		access_token = create_access_token(identity=user.id, fresh=True)
		refresh_token = create_refresh_token(user.id)

		return {
			       "access_token": access_token,
			       "refresh_token": refresh_token
		       }, 200
