from flask import request
from flask_jwt_extended import fresh_jwt_required, get_jwt_identity
from flask_restful import Resource
from werkzeug.security import generate_password_hash

from database.models.user import UserModel
from libs.lang import gettext
from . import user_schema


class SetPassword(Resource):
	@classmethod
	@fresh_jwt_required
	def post(cls):
		user_data = user_schema.load(request.get_json())
		user = UserModel.find_by_username(user_data.username)

		if not user:
			if not user:
				return {
					       "message": gettext("user", "USER_NOT_FOUND_ERROR"),
					       "error": "NOT_FOUND"
				       }, 404

		if user.id != get_jwt_identity():
			return {
				       "message": gettext("_app", "UNAUTHORIZED_ERROR").format(
					       "you can not change a different users password"),
				       "error": "UNAUTHORIZED_ERROR"
			       }, 403

		if user.password:
			return {
				       "message": gettext("user", "PASSWORD_NOT_EMPTY_ERROR"),
				       "error": "PASSWORD_NOT_EMPTY_ERROR"
			       }, 400

		user.password = generate_password_hash(password=user_data.password)
		user.save()

		return {"message": gettext("user", "PASSWORD_SET")}, 201
