"""
Not essential for the core of the app (users, tokens, ...) to work. Feel free to modify & delete. Don't forget to also
remove the model and schema.
"""

from flask_jwt_extended import jwt_required, fresh_jwt_required
from flask_restful import Resource

from database.models.item import ItemModel
from database.models.store import StoreModel
from libs.lang import gettext
from . import store_schema


class Store(Resource):
	@classmethod
	def get(cls, name: str):
		"""
		Retrieve an entity.
		:return:
		"""
		store = StoreModel.find_by_name(name)
		if store:
			return store_schema.dump(store), 200
		return {
			       "message": gettext("store", "STORE_NOT_FOUND_ERROR").format(name),
			       "error": "NOT_FOUND"
		       }, 404

	@classmethod
	@jwt_required
	def post(cls, name: str):
		"""
		Create an entity.
		:param name: Store name
		:return:
		"""
		if StoreModel.find_by_name(name):
			return {
				       "message": gettext("store", "STORE_EXISTS_ERROR").format(name),
				       "error": "ALREADY_EXISTS"
			       }, 400

		store = StoreModel(name=name)  # No __init__ method available, so column mapping (name=) is required.
		try:
			store.save()
		except:
			return {
				       "message": gettext("_app", "UNDEFINED_ERROR"),
				       "error": "UNDEFINED_ERROR"
			       }, 500

		return store_schema.dump(store), 201

	@classmethod
	@fresh_jwt_required
	def delete(cls, name: str):
		"""
		Delete an entity.
		:param name: Store name
		:return:
		"""
		store = StoreModel.find_by_name(name)

		if ItemModel.find_all_by_store_id(store.id):
			return {
				       "message": gettext("store", "STORE_HAS_ITEMS_ERROR"),
				       "error": "STORE_HAS_ITEMS"
			       }, 400

		if store:
			store.delete()

		return {
			       "message": gettext("store", "STORE_DELETED")
		       }, 200
