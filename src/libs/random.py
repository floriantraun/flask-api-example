"""
libs.random

Creates random string.
"""

import random
import string


def random_string(length: int, uppercase: bool = True, lowercase: bool = True, digits: bool = False) -> str:
	"""
	Returns a random string with the character sets defined.
	:param length: length of the string
	:param uppercase: include uppercase letters in the character set
	:param lowercase: include lowercase letters in the character set
	:param digits: include digits in the character set
	:return: str
	"""

	letters = ""

	if uppercase:
		letters += string.ascii_uppercase

	if lowercase:
		letters += string.ascii_lowercase

	if digits:
		letters += string.digits

	return ''.join(random.choice(letters) for _ in range(length))
