"""
libs.parser

Parses string for schemas like email addresses and more.
"""

import re
from typing import Union

EMAIL = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
FILENAME = r"^[a-zA-Z0-9][a-zA-Z0-9_()-\.\[\]\s]*\.({allowed_formats})$"


def parse(string: str, schema: str, **kwargs) -> Union[None, object]:
	"""
	Parses a string for defined schema.
	:param string: String to parse
	:param schema: Defined schema (such as email, tel, ...)
	:return: bool
	"""
	search = None

	if schema == "filename":
		search = re.search(string=string,
		                   pattern=FILENAME.format(allowed_formats=kwargs["allowed_formats"]))

	return search
