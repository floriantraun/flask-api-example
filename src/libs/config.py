"""
libs.config

By default, uses 'config.json' file inside the top-level directory.

If config changes on the fly, run `libs.config.refresh()`.
"""

import json
import pathlib

default_config = "config/config"  # Without .json extension
cached_strings = {}


def refresh() -> None:
	"""
	Takes the config file and loads it into cached_strings.
	:return: None
	"""
	global cached_strings
	filepath = pathlib.Path(__file__).parent.absolute().joinpath("..", f"{default_config}.json")

	with open(filepath, "r") as file:
		cached_strings = json.load(file)


def getconf(*argv: str) -> str:
	"""
	Returns the requested string in the config file.
	:param argv: String indexes, e.g. if you want the app/default_locale string:
	getconf("app", "default_locale")
	:return: str
	"""
	return_value = cached_strings
	for index in argv:
		if not index in return_value:
			return_value = ""
			break
		return_value = return_value[index]

	return return_value


refresh()
