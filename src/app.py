from flask import Flask
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from flask_restful import Api
from flask_uploads import configure_uploads, patch_request_class

from config.app import add_resources
from config.app import flask_methods_overwrite
from config.app import jwt_methods_overwrite
from database import db
from database.schemas import ma
from libs.config import getconf
from libs.oauth import oauth
from libs.upload_helper import FILE_SET

# Configure Flask and Flask Restful.
app = Flask(__name__, static_folder="static/")
app.config.from_object("config.app.flask_config")
patch_request_class(app, getconf("app", "max_upload_size"))  # Limit upload size; default is 10MB.
configure_uploads(app, FILE_SET)  # Activate uploads for the extension set.
api = Api(app)

# Initialize app.
db.init_app(app)

# Initialize Marshmallow.
ma.init_app(app)

# Initialize migrations.
migrate = Migrate(app, db)

# Initialize OAuth.
oauth.init_app(app)


# Run migrations before first request and create database (SQLite) if it doesn't exist.
@app.before_first_request
def create_tables():
	"""
	Runs migrations (creates tables).
	:return: None
	"""
	db.create_all()


# Overwrite Flask methods such as error handling.
flask_methods_overwrite(app)

# Initialize JSON-Web-Token (JWT).
jwt = JWTManager(app)

# Changing Flask-JWT default behaviors.
jwt_methods_overwrite(jwt)

# Add resources to API object.
add_resources(api)

if __name__ == "__main__":
	app.run(host=getconf("app", "host"), port=int(getconf("app", "port")))
